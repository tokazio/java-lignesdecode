### Lignes de code ###

Compte les lignes de codes (hors commentaires) des fichiers .java d'un dossier et ses sous dossier.

Utiliser l'invite de commande pour executer le programme.
Passer le nom du dossier dans lequel compte en 1er paramètre (entre guillemets)

```
#!java

>java -jar LigneDeCode.jar "chemin du dossier"
```

Depuis Netbeans (avec le plugin gradle):
Clic droit sur le projet, Custom tasks, Run

va executer:

```
#!java

gradle run -Pmyargs="dossier"
```