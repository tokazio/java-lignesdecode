package lignedecode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author romainpetit
 */
public class LigneDeCode extends Thread {

    private static final char EXTENSION_SEPARATOR = '.';
    private static final char DIRECTORY_SEPARATOR = '/';
    private static int nbLignes = 0;
    private static int nbFichiers = 0;

    private final File folder;

    public LigneDeCode(final File folder) {
        this.folder = folder;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 0) {
            System.out.println("Indiquer le dossier de recherche en premier paramètre.");
            return;
        }
        File folder = new File(args[0]);
        if (!folder.exists()) {
            System.out.println("Le dossier '" + folder.getAbsolutePath() + "' n'existe pas.");
            return;
        }
        System.out.println("Recherche dans: " + folder.getAbsolutePath());
        LigneDeCode t = new LigneDeCode(folder);
        t.start();
        t.join();
        System.out.println("Nombre total de fichiers (java): " + nbFichiers);
        System.out.println("Nombre total de ligne de code (réelles): " + nbLignes);
    }

    /**
     * Return the file extension from a filename, including the "."
     *
     * e.g. /path/to/myfile.jpg donne .jpg
     *
     * @param filename
     * @return
     */
    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }

        int index = indexOfExtension(filename);

        if (index == -1) {
            return filename;
        } else {
            return filename.substring(index);
        }
    }

    /**
     *
     * @param filename
     * @return
     */
    public static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        }
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        int lastDirSeparator = filename.lastIndexOf(DIRECTORY_SEPARATOR);
        if (lastDirSeparator > extensionPos) {
            System.out.println("A directory separator appears after the file extension, assuming there is no file extension");
            return -1;
        }
        return extensionPos;
    }

    @Override
    public void run() {
        try {
            get(folder);
        } catch (IOException ex) {
            Logger.getLogger(LigneDeCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void get(File folder) throws FileNotFoundException, IOException {
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            int l = 0;
            if (file.isFile() && getExtension(file.getAbsolutePath()).equals(".java")) {
                LigneDeCode.nbFichiers++;
                String text;
                boolean cmt = false;
                boolean nop = false;
                BufferedReader reader = new BufferedReader(new FileReader(file));
                while ((text = reader.readLine()) != null) {
                    if (text.trim().isEmpty()) {
                        continue;
                    }
                    if (text.trim().startsWith("//")) {
                        continue;
                    }
                    //annotations @ mise en automatique par netbeans
                    if (text.trim().startsWith("@")) {
                        continue;
                    }
                    //import automatiquement ajouté par netbeans
                    if (text.trim().startsWith("import")) {
                        continue;
                    }

                    if (text.trim().startsWith("/*")) {
                        cmt = true;
                    }
                    if (cmt && text.trim().endsWith("*/")) {
                        cmt = false;
                        continue;
                    }
                    if (!cmt && !nop) {
                        LigneDeCode.nbLignes++;
                        l++;
                    }
                }
            }
            if (file.isDirectory()) {
                get(file);
            }
        }
    }
}
